//
//  IMFileCache.h
//  YuanXinFileTransfer
//
//  Created by 晏德智 on 2016/12/20.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMFileCache : NSObject

@property (nonatomic, strong) NSString *cachePath;

+ (instancetype)shareInstance;

@end
