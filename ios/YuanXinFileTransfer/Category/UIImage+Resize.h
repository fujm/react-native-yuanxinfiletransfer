//
//  UIImage+Resize.h
//  YuanXinKit
//
//  Created by 晏德智 on 2016/11/3.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resize)

-(UIImage*)resizedImageToSize:(CGSize)dstSize;

-(UIImage*)resizedImageToFitInSize:(CGSize)boundingSize scaleIfSmaller:(BOOL)scale;

@end
