//
//  IFileTransferHandler.h
//  YuanXinFileTransfer
//
//  Created by 晏德智 on 2016/12/14.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileUploadInfo.h"

/**
 *  文件传输代理
 */
@protocol FileTransferProxyDelegate<NSObject>

@optional
- (BOOL)UploadFile:(FileUploadInfo *)fileInfo;

@end

/*
 * IM 特定传输数据代理
 */
@protocol IMFileTransferDelegate <NSObject>

@required

- (NSURL *)getHttpURL:(NSURL *)url;

- (BOOL)uploadFile:(FileUploadInfo *)fileInfo;

- (BOOL)downloadImage:(NSURL *)netUrl objectKey:(NSString *)objectKey;

- (BOOL)downloadAudioFile:(NSURL *)netUrl objectKey:(NSString *)objectKey;

- (void)downloadAudioFile:(NSURL *)netUrl
              toLocalPath:(NSString *)localPath
                  success:(void (^)(NSURL *fileURL))success
                  failure:(void (^)(NSError *error))failure;

- (void)downloadFile:(NSURL *)netUrl
         toLocalPath:(NSString *)localPath
             success:(void (^)(NSURL *fileURL))success
             failure:(void (^)(NSError *error))failure;

@end

@protocol IMFileTransferCallbackDelegate <NSObject>

@required
- (void)onError:(NSString *)tag error:(NSError *)error;

- (void)onUploadFileSucess:(NSString *)tag result:(id)result;

- (void)onDownloadFileSucess:(NSString *)tag result:(id)dataContent;

@end



