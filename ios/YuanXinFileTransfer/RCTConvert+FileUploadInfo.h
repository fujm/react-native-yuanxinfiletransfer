//
//  RCTConvert+FileUploadInfo.h
//  YuanXinFileTransfer
//
//  Created by 晏德智 on 2016/12/12.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#if __has_include(<React/RCTConvert.h>)
#import <React/RCTConvert.h>
#else
#import <React/RCTConvert.h>
#endif

#import "FileUploadInfo.h"

@interface RCTConvert (FileUploadInfo)

+ (FileUploadInfo *)FileUploadInfo:(id)json;

@end
