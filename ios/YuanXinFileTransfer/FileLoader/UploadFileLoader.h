//
//  FileLoader.h
//  YuanXinNetWorking
//
//  Created by 晏德智 on 2016/12/12.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileUploadInfo.h"

@interface UploadFileLoader : NSObject

+ (instancetype)shareInstance;

/**
 * 通过远薪的路径返回Data文件
 */
- (void)loaderFiles:(NSArray<FileUploadInfo *> *)files withCompleted:(void (^)(NSArray *results))completedBlock;

/**
 * 通过远薪的路径返回Data文件
 */
- (void)loaderFile:(FileUploadInfo *)fileUploadInfo
      successBlock:(void(^)(NSData *fileData,NSString *pathExtension))successBlock
      failureBlock:(void(^)(NSError *error))errorBlock;

/**
 * 拼一个Form提交文件的描术信息
 */
- (NSData *)appendFileFormData:(NSData *)fileData
                      fileName:(NSString *)fileName
                   contentType:(NSString *)contentType
                  formBoundary:(NSData *)formBoundaryData;

/**
 *  通过视频的URL，获得视频缩略图
 *  @param url 视频URL
 *  @return首帧缩略图
 */
#pragma mark 获取视频的首帧缩略图
- (UIImage *)imageWithVideoURL:(NSURL *)url;

/**
 * 通过后缀名处理ContentType
 */
- (NSString *)mimeTypeForFilePath:(NSString *)filePath;

static dispatch_queue_t currentMethodQueue();

@end
