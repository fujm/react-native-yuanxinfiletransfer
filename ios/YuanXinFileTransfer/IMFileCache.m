//
//  IMFileCache.m
//  YuanXinFileTransfer
//
//  Created by 晏德智 on 2016/12/20.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import "IMFileCache.h"
#import "SystemFileManager.h"

@interface IMFileCache()

@end

@implementation IMFileCache

+ (instancetype)shareInstance{
    static IMFileCache *_fileCache = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _fileCache = [[self alloc] init];
    });
    
    return _fileCache;
}

- (instancetype)init{
    self = [super init];
    if(self){
        NSURL *cachurl = [SystemFileManager baseDirForStorage:STORAGE_IMPORTANT];
        NSFileManager *fileManager =  [NSFileManager defaultManager];
        // NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        self.cachePath = [cachurl.absoluteString stringByAppendingPathComponent:@"file_cache"];
        if (![fileManager fileExistsAtPath:self.cachePath]) {
            [fileManager createDirectoryAtPath:self.cachePath withIntermediateDirectories:YES attributes:nil error:NULL];
        }
    }
    return self;
}

@end
