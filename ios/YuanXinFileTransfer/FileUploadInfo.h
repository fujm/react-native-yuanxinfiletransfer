//
//  FileUploadInfo.h
//  YuanXinFileTransfer
//
//  Created by 晏德智 on 2016/12/12.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, UpFileType) {
    UpFile =0,
    UpPhoto =1,
    UpVideo =2,
    UpAudio =3,
};

@interface FileUploadInfo : NSObject
/*
 * 本地文件路径
 */
@property (nonatomic, copy) NSURL   *filePath;

/*
 * 文件的类型
 */
@property (nonatomic, assign) UpFileType fileType;

/**
 * 大小比例压缩
 */
@property (nonatomic, assign) CGFloat scale;

/**
 * 质量比例压缩
 */
@property (nonatomic, assign) CGFloat quality;

@property (nonatomic, copy) NSString *objectKey;

@property (nonatomic, assign) CGSize size;

@end
