//
//  RCTConvert+FileUploadInfo.m
//  YuanXinFileTransfer
//
//  Created by 晏德智 on 2016/12/12.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import "RCTConvert+FileUploadInfo.h"

@implementation RCTConvert (FileUploadInfo)

RCT_ARRAY_CONVERTER(FileUploadInfo)

+ (FileUploadInfo *)FileUploadInfo:(id)json {
    
    FileUploadInfo *model = [FileUploadInfo new];
    //model.bucketName = [self NSString:json[@"bucket"]]?:@"Office";
    model.filePath = [self NSURL:json[@"filePath"]];
    model.fileType = [self NSNumber:json[@"type"]].integerValue?:UpPhoto;
    model.scale = @([self NSInteger:json[@"scale"]]).floatValue?:1;
    model.quality = [[self NSNumber:json[@"quality"]] floatValue] ? :0.4;
    model.objectKey = [self NSString:json[@"objectKey"]];
    //model.storageType = [self NSNumber:json[@"storageType"]].integerValue?:ByTime;
    CGFloat width = @([self NSInteger:json[@"width"]]).floatValue;
    CGFloat height = @([self NSInteger:json[@"height"]]).floatValue;
    model.size = CGSizeMake(width, height);
    if (!model.objectKey) {
        model.objectKey =  [[[NSUUID UUID] UUIDString] stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }
    
    return model;
}

@end
