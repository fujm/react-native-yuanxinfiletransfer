//
//  SystemFileManager.h
//  YuanXinKit
//
//  Created by 晏德智 on 2016/11/4.
//  Copyright © 2016年 晏德智. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const STORAGE_BACKED_UP;
extern NSString *const STORAGE_IMPORTANT;
extern NSString *const STORAGE_AUXILIARY;
extern NSString *const STORAGE_TEMPORARY;

@interface SystemFileManager : NSObject

+ (NSURL*)baseDirForStorage:(NSString*)storage;

+ (void)writeToFile:(NSString*)relativePath content:(NSString*)content inStorage:(NSString*)storage;
+ (NSString*)readFile:(NSString*)relativePath inStorage:(NSString*)storage error:(NSError**)error;
+ (BOOL)fileExists:(NSString*)relativePath inStorage:(NSString*)storage;
+ (BOOL)directoryExists:(NSString*)relativePath inStorage:(NSString*)storage;
+ (BOOL)deleteFileOrDirectory:(NSString*)relativePath inStorage:(NSString*)storage;
+ (NSString*)absolutePath:(NSString*)relativePath inStorage:(NSString*)storage;

// Extra method for integration from other modules / native code
+ (NSString*)moveFileFromUrl:(NSURL*)location toRelativePath:(NSString*)relativePath inStorage:(NSString*)storage;

@end
